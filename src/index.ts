import * as config from 'config';
import * as express from 'express';

import { routes } from './routes';
import { logger } from './utils';

const port = config.get('port');
const app = express() as Express;
app.use(express.json());

routes(app);

app.listen(port, err => {
	if (err) {
		throw err;
	}
	logger.info(`server is listening to ${port}`);
});
